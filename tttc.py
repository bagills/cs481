
# Import socket module 
import socket 
  
  
def Main(): 
    # local host IP '127.0.0.1' 
    host = 'gl.umbc.edu'
  
    # Define the port on which you want to connect 
    port = 13037
  
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM) 
  
    # connect to server on local computer 
    s.connect((host,port)) 
  
    while True:  
  
        # messaga received from server 
        data = s.recv(1024) 
  
        # print the received message 
        # here it would be a reverse of sent message 
        print(data) 
  
        # ask the client whether he wants to continue 
        pos = input()
		#s.send(pos)
    
	# close the connection 
    s.close() 
  
if __name__ == '__main__': 
    Main()
