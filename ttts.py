# import socket programming library 
import socket 
  
# import thread module 
from thread import *
import threading 
  
print_lock = threading.Lock() 

board = [" ", " ", " ", " ", " ", " ", " ", " ", " "]

winCombos = [(0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6), (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 4, 6)]
a = 0
gameOver = False
  
# thread fuction 
def threaded(c): 
    while True: 
		
		c.send("Tic Tac Toe\n")
		c.send("Player = X, Computer = O\n")
		printBoard()
		while not gameOver:
			if gameOver == True:
				c.close()
	
			c.send("Choose where to place your mark")
			playerTurn()
			printBoard()
			gameOver = checkBoard()
			if gameOver == True:
				c.close()

			c.send("It is now the computer's turn")
			computerTurn()
			printBoard()
			gameOver = checkBoard()
           
    # connection closed 
    c.close() 
  
	

def printBoard():
	c.send(board[0], board[1], board[2])
	c.send(board[3], board[4], board[5])
	c.send(board[6], board[7], board[8])


def playerTurn ():
	invalid = True
	while invalid:
		while invalid:
			a = c.recv(1024)
			try:
				a  = int(a)
				a -= 1
				if a in range(0, 9):
					if board[a] != " ":
						c.send("\nThat spot is taken. Try again")
						continue
					else:
						board[a] = "X"
						invalid = False
						continue
				else:
					c.send("\nThat's not on the board. Try again")
					continue
			except ValueError:
				c.send("\nThat's not a number. Try again")
			continue

def computerTurn():
	pos = 0
	while board[pos] != " ":
		pos = pos + 1
	board[pos] = "O"

def checkBoard():
	count = 0
	for i in winCombos:
		if board[i[0]] == board[i[1]] == board[i[2]] == "X":
			c.send("Player Wins!\n")
			c.send("Congratulations!\n")
			return True

		if board[i[0]] == board[i[1]] == board[i[2]] == "O":
			c.send("Computer Wins!\n")
			return True
	for a in range(9):
		if board[a] == "X" or board[a] == "O":
			count += 1
		if count == 9:
			c.send("The game ends in a Tie\n")
			return True




def Main(): 
    host = "" 
  
    # reverse a port on your computer 
    # in our case it is 12345 but it 
    # can be anything 
    port = 13037
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
    s.bind((host, port)) 
    print("socket binded to post", port) 
  
    # put the socket into listening mode 
    s.listen(5) 
    print("socket is listening") 
  
    # a forever loop until client wants to exit 
    while True: 
  
        # establish connection with client 
        c, addr = s.accept() 
  
        # lock acquired by client 
        print_lock.acquire() 
        print('Connected to :', addr[0], ':', addr[1]) 
  
        # Start a new thread and return its identifier 
        start_new_thread(threaded, (c,)) 
    s.close() 
  
  
if __name__ == '__main__': 
    Main() 
